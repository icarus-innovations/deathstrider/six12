### Six 12
---
This is an addon for Deathstrider that adds the Six12, a rotary, bullpup 12 gauge shotgun.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Accensus, coding and gauntlet sprites
- TSF, sprite assets

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.